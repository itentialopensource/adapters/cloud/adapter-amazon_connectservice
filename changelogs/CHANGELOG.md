
## 0.1.3 [03-09-2021]

* migration to the latest adapter foundation

See merge request itentialopensource/adapters/cloud/adapter-amazon_connectservice!1

---

## 0.1.2 [03-08-2021]

* migration to the latest adapter foundation

See merge request itentialopensource/adapters/cloud/adapter-amazon_connectservice!1

---

## 0.1.1 [03-08-2021]

* migration to the latest adapter foundation

See merge request itentialopensource/adapters/cloud/adapter-amazon_connectservice!1

---
