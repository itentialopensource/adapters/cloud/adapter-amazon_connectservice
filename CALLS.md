## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Amazon Connect. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Amazon Connect.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Amazon Connect. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">listContactFlowsSTSRole(instanceId, contactFlowTypes, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">Provides information about the contact flows for the specified Amazon Connect instance.   You can a</td>
    <td style="padding:15px">{base_path}/{version}/contact-flows-summary/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createContactFlowSTSRole(instanceId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a contact flow for the specified Amazon Connect instance.   You can also create and update</td>
    <td style="padding:15px">{base_path}/{version}/contact-flows/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeContactFlowSTSRole(instanceId, contactFlowId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the specified contact flow.   You can also create and update contact flows using the  Ama</td>
    <td style="padding:15px">{base_path}/{version}/contact-flows/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateContactFlowContentSTSRole(instanceId, contactFlowId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates the specified contact flow.   You can also create and update contact flows using the  Amazo</td>
    <td style="padding:15px">{base_path}/{version}/contact-flows/{pathv1}/{pathv2}/content?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateContactFlowNameSTSRole(instanceId, contactFlowId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">The name of the contact flow.   You can also create and update contact flows using the  Amazon Conn</td>
    <td style="padding:15px">{base_path}/{version}/contact-flows/{pathv1}/{pathv2}/name?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateContactAttributesSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates or updates the contact attributes associated with the specified contact.   You can add or u</td>
    <td style="padding:15px">{base_path}/{version}/contact/attributes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContactAttributesSTSRole(instanceId, initialContactId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Retrieves the contact attributes for the specified contact.</td>
    <td style="padding:15px">{base_path}/{version}/contact/attributes/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startChatContactSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Initiates a contact flow to start a new chat for the customer. Response of this API provides a toke</td>
    <td style="padding:15px">{base_path}/{version}/contact/chat?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startOutboundVoiceContactSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Places an outbound call to a contact, and then initiates the contact flow. It performs the actions</td>
    <td style="padding:15px">{base_path}/{version}/contact/outbound-voice?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resumeContactRecordingSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">When a contact is being recorded, and the recording has been suspended using SuspendContactRecordin</td>
    <td style="padding:15px">{base_path}/{version}/contact/resume-recording?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startContactRecordingSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Starts recording the contact when the agent joins the call. StartContactRecording is a one-time act</td>
    <td style="padding:15px">{base_path}/{version}/contact/start-recording?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stopContactSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Ends the specified contact.</td>
    <td style="padding:15px">{base_path}/{version}/contact/stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stopContactRecordingSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Stops recording a call when a contact is being recorded. StopContactRecording is a one-time action.</td>
    <td style="padding:15px">{base_path}/{version}/contact/stop-recording?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">suspendContactRecordingSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">When a contact is being recorded, this API suspends recording the call. For example, you might susp</td>
    <td style="padding:15px">{base_path}/{version}/contact/suspend-recording?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startTaskContactSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Initiates a contact flow to start a new task.</td>
    <td style="padding:15px">{base_path}/{version}/contact/task?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listHoursOfOperationsSTSRole(instanceId, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">Provides information about the hours of operation for the specified Amazon Connect instance.   For</td>
    <td style="padding:15px">{base_path}/{version}/hours-of-operations-summary/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeHoursOfOperationSTSRole(instanceId, hoursOfOperationId, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Describes the hours o</td>
    <td style="padding:15px">{base_path}/{version}/hours-of-operations/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listInstancesSTSRole(nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Return a list of inst</td>
    <td style="padding:15px">{base_path}/{version}/instance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createInstanceSTSRole(body, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Initiates an Amazon C</td>
    <td style="padding:15px">{base_path}/{version}/instance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInstanceSTSRole(instanceId, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Deletes the Amazon Co</td>
    <td style="padding:15px">{base_path}/{version}/instance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeInstanceSTSRole(instanceId, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Returns the current s</td>
    <td style="padding:15px">{base_path}/{version}/instance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateApprovedOriginSTSRole(instanceId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Associates an approve</td>
    <td style="padding:15px">{base_path}/{version}/instance/{pathv1}/approved-origin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disassociateApprovedOriginSTSRole(instanceId, origin, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Revokes access to int</td>
    <td style="padding:15px">{base_path}/{version}/instance/{pathv1}/approved-origin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listApprovedOriginsSTSRole(instanceId, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Returns a paginated l</td>
    <td style="padding:15px">{base_path}/{version}/instance/{pathv1}/approved-origins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeInstanceAttributeSTSRole(instanceId, attributeType = 'INBOUND_CALLS', stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Describes the specifi</td>
    <td style="padding:15px">{base_path}/{version}/instance/{pathv1}/attribute/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateInstanceAttributeSTSRole(instanceId, attributeType = 'INBOUND_CALLS', body, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Updates the value for</td>
    <td style="padding:15px">{base_path}/{version}/instance/{pathv1}/attribute/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listInstanceAttributesSTSRole(instanceId, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Returns a paginated l</td>
    <td style="padding:15px">{base_path}/{version}/instance/{pathv1}/attributes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listIntegrationAssociationsSTSRole(instanceId, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Provides summary info</td>
    <td style="padding:15px">{base_path}/{version}/instance/{pathv1}/integration-associations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIntegrationAssociationSTSRole(instanceId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Create an AppIntegrat</td>
    <td style="padding:15px">{base_path}/{version}/instance/{pathv1}/integration-associations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIntegrationAssociationSTSRole(instanceId, integrationAssociationId, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Deletes an AppIntegra</td>
    <td style="padding:15px">{base_path}/{version}/instance/{pathv1}/integration-associations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUseCasesSTSRole(instanceId, integrationAssociationId, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Lists the use cases.</td>
    <td style="padding:15px">{base_path}/{version}/instance/{pathv1}/integration-associations/{pathv2}/use-cases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUseCaseSTSRole(instanceId, integrationAssociationId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Creates a use case fo</td>
    <td style="padding:15px">{base_path}/{version}/instance/{pathv1}/integration-associations/{pathv2}/use-cases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUseCaseSTSRole(instanceId, integrationAssociationId, useCaseId, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Deletes a use case fr</td>
    <td style="padding:15px">{base_path}/{version}/instance/{pathv1}/integration-associations/{pathv2}/use-cases/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateLambdaFunctionSTSRole(instanceId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Allows the specified</td>
    <td style="padding:15px">{base_path}/{version}/instance/{pathv1}/lambda-function?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disassociateLambdaFunctionSTSRole(instanceId, functionArn, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Remove the Lambda fun</td>
    <td style="padding:15px">{base_path}/{version}/instance/{pathv1}/lambda-function?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listLambdaFunctionsSTSRole(instanceId, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Returns a paginated l</td>
    <td style="padding:15px">{base_path}/{version}/instance/{pathv1}/lambda-functions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateLexBotSTSRole(instanceId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Allows the specified</td>
    <td style="padding:15px">{base_path}/{version}/instance/{pathv1}/lex-bot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disassociateLexBotSTSRole(instanceId, botName, lexRegion, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Revokes authorization</td>
    <td style="padding:15px">{base_path}/{version}/instance/{pathv1}/lex-bot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listLexBotsSTSRole(instanceId, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Returns a paginated l</td>
    <td style="padding:15px">{base_path}/{version}/instance/{pathv1}/lex-bots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateSecurityKeySTSRole(instanceId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Associates a security</td>
    <td style="padding:15px">{base_path}/{version}/instance/{pathv1}/security-key?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disassociateSecurityKeySTSRole(instanceId, associationId, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Deletes the specified</td>
    <td style="padding:15px">{base_path}/{version}/instance/{pathv1}/security-key/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSecurityKeysSTSRole(instanceId, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Returns a paginated l</td>
    <td style="padding:15px">{base_path}/{version}/instance/{pathv1}/security-keys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateInstanceStorageConfigSTSRole(instanceId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Associates a storage</td>
    <td style="padding:15px">{base_path}/{version}/instance/{pathv1}/storage-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disassociateInstanceStorageConfigSTSRole(instanceId, associationId, resourceType = 'CHAT_TRANSCRIPTS', stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Removes the storage t</td>
    <td style="padding:15px">{base_path}/{version}/instance/{pathv1}/storage-config/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeInstanceStorageConfigSTSRole(instanceId, associationId, resourceType = 'CHAT_TRANSCRIPTS', stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Retrieves the current</td>
    <td style="padding:15px">{base_path}/{version}/instance/{pathv1}/storage-config/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateInstanceStorageConfigSTSRole(instanceId, associationId, resourceType = 'CHAT_TRANSCRIPTS', body, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Updates an existing c</td>
    <td style="padding:15px">{base_path}/{version}/instance/{pathv1}/storage-config/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listInstanceStorageConfigsSTSRole(instanceId, resourceType = 'CHAT_TRANSCRIPTS', nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Returns a paginated l</td>
    <td style="padding:15px">{base_path}/{version}/instance/{pathv1}/storage-configs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCurrentMetricDataSTSRole(instanceId, maxResults, nextToken, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets the real-time metric data from the specified Amazon Connect instance.   For a description of e</td>
    <td style="padding:15px">{base_path}/{version}/metrics/current/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMetricDataSTSRole(instanceId, maxResults, nextToken, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets historical metric data from the specified Amazon Connect instance.   For a description of each</td>
    <td style="padding:15px">{base_path}/{version}/metrics/historical/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPhoneNumbersSTSRole(instanceId, phoneNumberTypes, phoneNumberCountryCodes, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">Provides information about the phone numbers for the specified Amazon Connect instance.    For more</td>
    <td style="padding:15px">{base_path}/{version}/phone-numbers-summary/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPromptsSTSRole(instanceId, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">Provides information about the prompts for the specified Amazon Connect instance.</td>
    <td style="padding:15px">{base_path}/{version}/prompts-summary/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listQueuesSTSRole(instanceId, queueTypes, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">Provides information about the queues for the specified Amazon Connect instance.   For more informa</td>
    <td style="padding:15px">{base_path}/{version}/queues-summary/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createQueueSTSRole(instanceId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Creates a new queue f</td>
    <td style="padding:15px">{base_path}/{version}/queues/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeQueueSTSRole(instanceId, queueId, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Describes the specifi</td>
    <td style="padding:15px">{base_path}/{version}/queues/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateQueueQuickConnectsSTSRole(instanceId, queueId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Associates a set of q</td>
    <td style="padding:15px">{base_path}/{version}/queues/{pathv1}/{pathv2}/associate-quick-connects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disassociateQueueQuickConnectsSTSRole(instanceId, queueId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Disassociates a set o</td>
    <td style="padding:15px">{base_path}/{version}/queues/{pathv1}/{pathv2}/disassociate-quick-connects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateQueueHoursOfOperationSTSRole(instanceId, queueId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Updates the hours of</td>
    <td style="padding:15px">{base_path}/{version}/queues/{pathv1}/{pathv2}/hours-of-operation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateQueueMaxContactsSTSRole(instanceId, queueId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Updates the maximum n</td>
    <td style="padding:15px">{base_path}/{version}/queues/{pathv1}/{pathv2}/max-contacts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateQueueNameSTSRole(instanceId, queueId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Updates the name and</td>
    <td style="padding:15px">{base_path}/{version}/queues/{pathv1}/{pathv2}/name?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateQueueOutboundCallerConfigSTSRole(instanceId, queueId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Updates the outbound</td>
    <td style="padding:15px">{base_path}/{version}/queues/{pathv1}/{pathv2}/outbound-caller-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listQueueQuickConnectsSTSRole(instanceId, queueId, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Lists the quick conne</td>
    <td style="padding:15px">{base_path}/{version}/queues/{pathv1}/{pathv2}/quick-connects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateQueueStatusSTSRole(instanceId, queueId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Updates the status of</td>
    <td style="padding:15px">{base_path}/{version}/queues/{pathv1}/{pathv2}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listQuickConnectsSTSRole(instanceId, nextToken, maxResults, quickConnectTypes, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Provides information</td>
    <td style="padding:15px">{base_path}/{version}/quick-connects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createQuickConnectSTSRole(instanceId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Creates a quick conne</td>
    <td style="padding:15px">{base_path}/{version}/quick-connects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteQuickConnectSTSRole(instanceId, quickConnectId, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Deletes a quick conne</td>
    <td style="padding:15px">{base_path}/{version}/quick-connects/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeQuickConnectSTSRole(instanceId, quickConnectId, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Describes the quick c</td>
    <td style="padding:15px">{base_path}/{version}/quick-connects/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateQuickConnectConfigSTSRole(instanceId, quickConnectId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Updates the configura</td>
    <td style="padding:15px">{base_path}/{version}/quick-connects/{pathv1}/{pathv2}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateQuickConnectNameSTSRole(instanceId, quickConnectId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">This API is in preview release for Amazon Connect and is subject to change.   Updates the name and</td>
    <td style="padding:15px">{base_path}/{version}/quick-connects/{pathv1}/{pathv2}/name?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRoutingProfilesSTSRole(instanceId, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">Provides summary information about the routing profiles for the specified Amazon Connect instance.</td>
    <td style="padding:15px">{base_path}/{version}/routing-profiles-summary/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRoutingProfileSTSRole(instanceId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a new routing profile.</td>
    <td style="padding:15px">{base_path}/{version}/routing-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeRoutingProfileSTSRole(instanceId, routingProfileId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the specified routing profile.</td>
    <td style="padding:15px">{base_path}/{version}/routing-profiles/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateRoutingProfileQueuesSTSRole(instanceId, routingProfileId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Associates a set of queues with a routing profile.</td>
    <td style="padding:15px">{base_path}/{version}/routing-profiles/{pathv1}/{pathv2}/associate-queues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRoutingProfileConcurrencySTSRole(instanceId, routingProfileId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates the channels that agents can handle in the Contact Control Panel (CCP) for a routing profil</td>
    <td style="padding:15px">{base_path}/{version}/routing-profiles/{pathv1}/{pathv2}/concurrency?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRoutingProfileDefaultOutboundQueueSTSRole(instanceId, routingProfileId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates the default outbound queue of a routing profile.</td>
    <td style="padding:15px">{base_path}/{version}/routing-profiles/{pathv1}/{pathv2}/default-outbound-queue?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disassociateRoutingProfileQueuesSTSRole(instanceId, routingProfileId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Disassociates a set of queues from a routing profile.</td>
    <td style="padding:15px">{base_path}/{version}/routing-profiles/{pathv1}/{pathv2}/disassociate-queues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRoutingProfileNameSTSRole(instanceId, routingProfileId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates the name and description of a routing profile. The request accepts the following data in JS</td>
    <td style="padding:15px">{base_path}/{version}/routing-profiles/{pathv1}/{pathv2}/name?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRoutingProfileQueuesSTSRole(instanceId, routingProfileId, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">Lists the queues associated with a routing profile.</td>
    <td style="padding:15px">{base_path}/{version}/routing-profiles/{pathv1}/{pathv2}/queues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRoutingProfileQueuesSTSRole(instanceId, routingProfileId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates the properties associated with a set of queues for a routing profile.</td>
    <td style="padding:15px">{base_path}/{version}/routing-profiles/{pathv1}/{pathv2}/queues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSecurityProfilesSTSRole(instanceId, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">Provides summary information about the security profiles for the specified Amazon Connect instance.</td>
    <td style="padding:15px">{base_path}/{version}/security-profiles-summary/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTagsForResourceSTSRole(resourceArn, stsParams, roleName, callback)</td>
    <td style="padding:15px">Lists the tags for the specified resource.   For sample policies that use tags, see  Amazon Connect</td>
    <td style="padding:15px">{base_path}/{version}/tags/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tagResourceSTSRole(resourceArn, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Adds the specified tags to the specified resource.   The supported resource types are users, routin</td>
    <td style="padding:15px">{base_path}/{version}/tags/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">untagResourceSTSRole(resourceArn, tagKeys, stsParams, roleName, callback)</td>
    <td style="padding:15px">Removes the specified tags from the specified resource.</td>
    <td style="padding:15px">{base_path}/{version}/tags/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUserHierarchyGroupsSTSRole(instanceId, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">Provides summary information about the hierarchy groups for the specified Amazon Connect instance.</td>
    <td style="padding:15px">{base_path}/{version}/user-hierarchy-groups-summary/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUserHierarchyGroupSTSRole(instanceId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a new user hierarchy group.</td>
    <td style="padding:15px">{base_path}/{version}/user-hierarchy-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUserHierarchyGroupSTSRole(hierarchyGroupId, instanceId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes an existing user hierarchy group. It must not be associated with any agents or have any act</td>
    <td style="padding:15px">{base_path}/{version}/user-hierarchy-groups/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeUserHierarchyGroupSTSRole(hierarchyGroupId, instanceId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the specified hierarchy group.</td>
    <td style="padding:15px">{base_path}/{version}/user-hierarchy-groups/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUserHierarchyGroupNameSTSRole(hierarchyGroupId, instanceId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates the name of the user hierarchy group.</td>
    <td style="padding:15px">{base_path}/{version}/user-hierarchy-groups/{pathv1}/{pathv2}/name?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeUserHierarchyStructureSTSRole(instanceId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the hierarchy structure of the specified Amazon Connect instance.</td>
    <td style="padding:15px">{base_path}/{version}/user-hierarchy-structure/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUserHierarchyStructureSTSRole(instanceId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates the user hierarchy structure: add, remove, and rename user hierarchy levels.</td>
    <td style="padding:15px">{base_path}/{version}/user-hierarchy-structure/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFederationTokenSTSRole(instanceId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Retrieves a token for federation.</td>
    <td style="padding:15px">{base_path}/{version}/user/federate/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUsersSTSRole(instanceId, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">Provides summary information about the users for the specified Amazon Connect instance.</td>
    <td style="padding:15px">{base_path}/{version}/users-summary/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUserSTSRole(instanceId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a user account for the specified Amazon Connect instance.   For information about how to cr</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUserSTSRole(instanceId, userId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes a user account from the specified Amazon Connect instance.   For information about what hap</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeUserSTSRole(userId, instanceId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the specified user account. You can find the instance ID in the console (it’s the final p</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUserHierarchySTSRole(userId, instanceId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Assigns the specified hierarchy group to the specified user.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/{pathv2}/hierarchy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUserIdentityInfoSTSRole(userId, instanceId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates the identity information for the specified user.     We strongly recommend limiting who has</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/{pathv2}/identity-info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUserPhoneConfigSTSRole(userId, instanceId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates the phone configuration settings for the specified user.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/{pathv2}/phone-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUserRoutingProfileSTSRole(userId, instanceId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Assigns the specified routing profile to the specified user.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/{pathv2}/routing-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUserSecurityProfilesSTSRole(userId, instanceId, body, stsParams, roleName, callback)</td>
    <td style="padding:15px">Assigns the specified security profiles to the specified user.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/{pathv2}/security-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>