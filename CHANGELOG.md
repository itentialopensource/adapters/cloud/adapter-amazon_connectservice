
## 0.4.8 [11-12-2024]

* more auth changes

See merge request itentialopensource/adapters/adapter-amazon_connectservice!20

---

## 0.4.7 [10-15-2024]

* Changes made at 2024.10.14_20:39PM

See merge request itentialopensource/adapters/adapter-amazon_connectservice!19

---

## 0.4.6 [09-30-2024]

* update auth docs

See merge request itentialopensource/adapters/adapter-amazon_connectservice!17

---

## 0.4.5 [09-12-2024]

* add properties for sts

See merge request itentialopensource/adapters/adapter-amazon_connectservice!16

---

## 0.4.4 [08-22-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-amazon_connectservice!15

---

## 0.4.3 [08-14-2024]

* Changes made at 2024.08.14_18:54PM

See merge request itentialopensource/adapters/adapter-amazon_connectservice!14

---

## 0.4.2 [08-07-2024]

* Changes made at 2024.08.06_20:07PM

See merge request itentialopensource/adapters/adapter-amazon_connectservice!13

---

## 0.4.1 [08-05-2024]

* Changes made at 2024.08.05_19:16PM

See merge request itentialopensource/adapters/adapter-amazon_connectservice!12

---

## 0.4.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-amazon_connectservice!11

---

## 0.3.8 [03-28-2024]

* Changes made at 2024.03.28_13:20PM

See merge request itentialopensource/adapters/cloud/adapter-amazon_connectservice!10

---

## 0.3.7 [03-21-2024]

* Changes made at 2024.03.21_13:49PM

See merge request itentialopensource/adapters/cloud/adapter-amazon_connectservice!9

---

## 0.3.6 [03-15-2024]

* Update metadata.json

See merge request itentialopensource/adapters/cloud/adapter-amazon_connectservice!8

---

## 0.3.5 [03-13-2024]

* Changes made at 2024.03.13_11:44AM

See merge request itentialopensource/adapters/cloud/adapter-amazon_connectservice!7

---

## 0.3.4 [03-11-2024]

* Changes made at 2024.03.11_15:34PM

See merge request itentialopensource/adapters/cloud/adapter-amazon_connectservice!6

---

## 0.3.3 [02-28-2024]

* Changes made at 2024.02.28_11:48AM

See merge request itentialopensource/adapters/cloud/adapter-amazon_connectservice!5

---

## 0.3.2 [01-27-2024]

* added dynamic region support

See merge request itentialopensource/adapters/cloud/adapter-amazon_connectservice!4

---

## 0.3.1 [12-26-2023]

* update axios and metadata

See merge request itentialopensource/adapters/cloud/adapter-amazon_connectservice!3

---

## 0.3.0 [12-14-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-amazon_connectservice!2

---

## 0.2.0 [11-29-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-amazon_connectservice!2

---
