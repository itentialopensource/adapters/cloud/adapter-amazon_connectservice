/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-amazon_connectservice',
      type: 'AmazonConnectService',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const AmazonConnectService = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] AmazonConnectService Adapter Test', () => {
  describe('AmazonConnectService Class Tests', () => {
    const a = new AmazonConnectService(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const contactFlowsSummaryInstanceId = 'fakedata';
    describe('#listContactFlows - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listContactFlows(contactFlowsSummaryInstanceId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.ContactFlowSummaryList));
                assert.equal('string', typeof data.response.nextToken);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ContactFlowsSummary', 'listContactFlows', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const contactFlowsInstanceId = 'fakedata';
    const contactFlowsContactFlowId = 'fakedata';
    const contactFlowsUpdateContactFlowContentBodyParam = {
      Content: 'string'
    };
    describe('#updateContactFlowContent - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateContactFlowContent(contactFlowsInstanceId, contactFlowsContactFlowId, contactFlowsUpdateContactFlowContentBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ContactFlows', 'updateContactFlowContent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const contactFlowsUpdateContactFlowNameBodyParam = {
      Description: 'string',
      Name: 'string'
    };
    describe('#updateContactFlowName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateContactFlowName(contactFlowsInstanceId, contactFlowsContactFlowId, contactFlowsUpdateContactFlowNameBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ContactFlows', 'updateContactFlowName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const contactFlowsCreateContactFlowBodyParam = {
      Content: 'string',
      Name: 'string',
      Type: 'CUSTOMER_HOLD'
    };
    describe('#createContactFlow - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createContactFlow(contactFlowsInstanceId, contactFlowsCreateContactFlowBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ContactFlows', 'createContactFlow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeContactFlow - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeContactFlow(contactFlowsInstanceId, contactFlowsContactFlowId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.ContactFlow);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ContactFlows', 'describeContactFlow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const contactUpdateContactAttributesBodyParam = {
      Attributes: {},
      InitialContactId: 'string',
      InstanceId: 'string'
    };
    describe('#updateContactAttributes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateContactAttributes(contactUpdateContactAttributesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Contact', 'updateContactAttributes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const contactResumeContactRecordingBodyParam = {
      ContactId: 'string',
      InitialContactId: 'string',
      InstanceId: 'string'
    };
    describe('#resumeContactRecording - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.resumeContactRecording(contactResumeContactRecordingBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Contact', 'resumeContactRecording', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const contactStartContactRecordingBodyParam = {
      ContactId: 'string',
      InitialContactId: 'string',
      InstanceId: 'string',
      VoiceRecordingConfiguration: {}
    };
    describe('#startContactRecording - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.startContactRecording(contactStartContactRecordingBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Contact', 'startContactRecording', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const contactStopContactBodyParam = {
      ContactId: 'string',
      InstanceId: 'string'
    };
    describe('#stopContact - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.stopContact(contactStopContactBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Contact', 'stopContact', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const contactStopContactRecordingBodyParam = {
      ContactId: 'string',
      InitialContactId: 'string',
      InstanceId: 'string'
    };
    describe('#stopContactRecording - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.stopContactRecording(contactStopContactRecordingBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Contact', 'stopContactRecording', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const contactSuspendContactRecordingBodyParam = {
      ContactId: 'string',
      InitialContactId: 'string',
      InstanceId: 'string'
    };
    describe('#suspendContactRecording - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.suspendContactRecording(contactSuspendContactRecordingBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Contact', 'suspendContactRecording', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const contactInstanceId = 'fakedata';
    const contactInitialContactId = 'fakedata';
    describe('#getContactAttributes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getContactAttributes(contactInstanceId, contactInitialContactId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.Attributes);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Contact', 'getContactAttributes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const contactStartChatContactBodyParam = {
      ContactFlowId: 'string',
      InstanceId: 'string',
      ParticipantDetails: {}
    };
    describe('#startChatContact - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.startChatContact(contactStartChatContactBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Contact', 'startChatContact', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const contactStartOutboundVoiceContactBodyParam = {
      ContactFlowId: 'string',
      DestinationPhoneNumber: 'string',
      InstanceId: 'string'
    };
    describe('#startOutboundVoiceContact - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.startOutboundVoiceContact(contactStartOutboundVoiceContactBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Contact', 'startOutboundVoiceContact', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const contactStartTaskContactBodyParam = {
      ContactFlowId: 'string',
      InstanceId: 'string',
      Name: 'string'
    };
    describe('#startTaskContact - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.startTaskContact(contactStartTaskContactBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Contact', 'startTaskContact', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const hoursOfOperationsSummaryInstanceId = 'fakedata';
    describe('#listHoursOfOperations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listHoursOfOperations(hoursOfOperationsSummaryInstanceId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.HoursOfOperationSummaryList));
                assert.equal('string', typeof data.response.nextToken);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HoursOfOperationsSummary', 'listHoursOfOperations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const hoursOfOperationsInstanceId = 'fakedata';
    const hoursOfOperationsHoursOfOperationId = 'fakedata';
    describe('#describeHoursOfOperation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeHoursOfOperation(hoursOfOperationsInstanceId, hoursOfOperationsHoursOfOperationId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.HoursOfOperation);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HoursOfOperations', 'describeHoursOfOperation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const instanceInstanceId = 'fakedata';
    const instanceAttributeType = 'fakedata';
    const instanceUpdateInstanceAttributeBodyParam = {
      Value: 'string'
    };
    describe('#updateInstanceAttribute - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateInstanceAttribute(instanceInstanceId, instanceAttributeType, instanceUpdateInstanceAttributeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instance', 'updateInstanceAttribute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const instanceAssociationId = 'fakedata';
    const instanceResourceType = 'fakedata';
    const instanceUpdateInstanceStorageConfigBodyParam = {
      StorageConfig: {}
    };
    describe('#updateInstanceStorageConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateInstanceStorageConfig(instanceInstanceId, instanceAssociationId, instanceResourceType, instanceUpdateInstanceStorageConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instance', 'updateInstanceStorageConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const instanceCreateInstanceBodyParam = {
      IdentityManagementType: 'EXISTING_DIRECTORY',
      InboundCallsEnabled: true,
      OutboundCallsEnabled: false
    };
    describe('#createInstance - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createInstance(instanceCreateInstanceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instance', 'createInstance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listInstances - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listInstances(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.InstanceSummaryList));
                assert.equal('string', typeof data.response.nextToken);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instance', 'listInstances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeInstance - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeInstance(instanceInstanceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.Instance);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instance', 'describeInstance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const instanceAssociateApprovedOriginBodyParam = {
      Origin: 'string'
    };
    describe('#associateApprovedOrigin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.associateApprovedOrigin(instanceInstanceId, instanceAssociateApprovedOriginBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instance', 'associateApprovedOrigin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listApprovedOrigins - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listApprovedOrigins(instanceInstanceId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', typeof data.response.nextToken);
                assert.equal(true, Array.isArray(data.response.Origins));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instance', 'listApprovedOrigins', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeInstanceAttribute - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeInstanceAttribute(instanceInstanceId, instanceAttributeType, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.Attribute);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instance', 'describeInstanceAttribute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listInstanceAttributes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listInstanceAttributes(instanceInstanceId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.Attributes));
                assert.equal('string', typeof data.response.nextToken);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instance', 'listInstanceAttributes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let instanceIntegrationAssociationId = 'fakedata';
    const instanceCreateIntegrationAssociationBodyParam = {
      IntegrationArn: 'string',
      IntegrationType: 'EVENT',
      SourceApplicationName: 'string',
      SourceApplicationUrl: 'string',
      SourceType: 'ZENDESK'
    };
    describe('#createIntegrationAssociation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createIntegrationAssociation(instanceInstanceId, instanceCreateIntegrationAssociationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              instanceIntegrationAssociationId = data.response.integrationAssociationId;
              saveMockData('Instance', 'createIntegrationAssociation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listIntegrationAssociations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listIntegrationAssociations(instanceInstanceId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.IntegrationAssociationSummaryList));
                assert.equal('string', typeof data.response.nextToken);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instance', 'listIntegrationAssociations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let instanceUseCaseId = 'fakedata';
    const instanceCreateUseCaseBodyParam = {
      UseCaseType: 'RULES_EVALUATION'
    };
    describe('#createUseCase - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createUseCase(instanceInstanceId, instanceIntegrationAssociationId, instanceCreateUseCaseBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              instanceUseCaseId = data.response.useCaseId;
              saveMockData('Instance', 'createUseCase', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listUseCases - errors', () => {
      it('listUseCases should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listUseCases(instanceInstanceId, instanceIntegrationAssociationId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', typeof data.response.nextToken);
                assert.equal(true, Array.isArray(data.response.UseCaseSummaryList));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instance', 'listUseCases', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const instanceAssociateLambdaFunctionBodyParam = {
      FunctionArn: 'string'
    };
    describe('#associateLambdaFunction - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.associateLambdaFunction(instanceInstanceId, instanceAssociateLambdaFunctionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instance', 'associateLambdaFunction', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listLambdaFunctions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listLambdaFunctions(instanceInstanceId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.LambdaFunctions));
                assert.equal('string', typeof data.response.nextToken);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instance', 'listLambdaFunctions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const instanceAssociateLexBotBodyParam = {
      LexBot: {}
    };
    describe('#associateLexBot - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.associateLexBot(instanceInstanceId, instanceAssociateLexBotBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instance', 'associateLexBot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listLexBots - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listLexBots(instanceInstanceId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.LexBots));
                assert.equal('string', typeof data.response.nextToken);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instance', 'listLexBots', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const instanceAssociateSecurityKeyBodyParam = {
      Key: 'string'
    };
    describe('#associateSecurityKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.associateSecurityKey(instanceInstanceId, instanceAssociateSecurityKeyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instance', 'associateSecurityKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSecurityKeys - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listSecurityKeys(instanceInstanceId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', typeof data.response.nextToken);
                assert.equal(true, Array.isArray(data.response.SecurityKeys));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instance', 'listSecurityKeys', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const instanceAssociateInstanceStorageConfigBodyParam = {
      ResourceType: 'AGENT_EVENTS',
      StorageConfig: {}
    };
    describe('#associateInstanceStorageConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.associateInstanceStorageConfig(instanceInstanceId, instanceAssociateInstanceStorageConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instance', 'associateInstanceStorageConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeInstanceStorageConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeInstanceStorageConfig(instanceInstanceId, instanceAssociationId, instanceResourceType, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.StorageConfig);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instance', 'describeInstanceStorageConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listInstanceStorageConfigs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listInstanceStorageConfigs(instanceInstanceId, instanceResourceType, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', typeof data.response.nextToken);
                assert.equal(true, Array.isArray(data.response.StorageConfigs));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instance', 'listInstanceStorageConfigs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metricsInstanceId = 'fakedata';
    const metricsGetCurrentMetricDataBodyParam = {
      CurrentMetrics: [
        {}
      ],
      Filters: {}
    };
    describe('#getCurrentMetricData - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCurrentMetricData(metricsInstanceId, null, null, metricsGetCurrentMetricDataBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('number', typeof data.response.DataSnapshotTime);
                assert.equal(true, Array.isArray(data.response.MetricResults));
                assert.equal('string', typeof data.response.nextToken);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Metrics', 'getCurrentMetricData', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const metricsGetMetricDataBodyParam = {
      EndTime: 'string',
      Filters: {},
      HistoricalMetrics: [
        {}
      ],
      StartTime: 'string'
    };
    describe('#getMetricData - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMetricData(metricsInstanceId, null, null, metricsGetMetricDataBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.MetricResults);
                assert.equal('string', typeof data.response.nextToken);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Metrics', 'getMetricData', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const phoneNumbersSummaryInstanceId = 'fakedata';
    describe('#listPhoneNumbers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listPhoneNumbers(phoneNumbersSummaryInstanceId, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', typeof data.response.nextToken);
                assert.equal(true, Array.isArray(data.response.PhoneNumberSummaryList));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PhoneNumbersSummary', 'listPhoneNumbers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const promptsSummaryInstanceId = 'fakedata';
    describe('#listPrompts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listPrompts(promptsSummaryInstanceId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', typeof data.response.nextToken);
                assert.equal(true, Array.isArray(data.response.PromptSummaryList));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PromptsSummary', 'listPrompts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const queuesSummaryInstanceId = 'fakedata';
    describe('#listQueues - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listQueues(queuesSummaryInstanceId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', typeof data.response.nextToken);
                assert.equal(true, Array.isArray(data.response.QueueSummaryList));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('QueuesSummary', 'listQueues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const queuesInstanceId = 'fakedata';
    const queuesQueueId = 'fakedata';
    const queuesAssociateQueueQuickConnectsBodyParam = {
      QuickConnectIds: [
        'string'
      ]
    };
    describe('#associateQueueQuickConnects - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.associateQueueQuickConnects(queuesInstanceId, queuesQueueId, queuesAssociateQueueQuickConnectsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Queues', 'associateQueueQuickConnects', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const queuesDisassociateQueueQuickConnectsBodyParam = {
      QuickConnectIds: [
        'string'
      ]
    };
    describe('#disassociateQueueQuickConnects - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.disassociateQueueQuickConnects(queuesInstanceId, queuesQueueId, queuesDisassociateQueueQuickConnectsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Queues', 'disassociateQueueQuickConnects', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const queuesUpdateQueueHoursOfOperationBodyParam = {
      HoursOfOperationId: 'string'
    };
    describe('#updateQueueHoursOfOperation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateQueueHoursOfOperation(queuesInstanceId, queuesQueueId, queuesUpdateQueueHoursOfOperationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Queues', 'updateQueueHoursOfOperation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const queuesUpdateQueueMaxContactsBodyParam = {
      MaxContacts: 3
    };
    describe('#updateQueueMaxContacts - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateQueueMaxContacts(queuesInstanceId, queuesQueueId, queuesUpdateQueueMaxContactsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Queues', 'updateQueueMaxContacts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const queuesUpdateQueueNameBodyParam = {
      Description: 'string',
      Name: 'string'
    };
    describe('#updateQueueName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateQueueName(queuesInstanceId, queuesQueueId, queuesUpdateQueueNameBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Queues', 'updateQueueName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const queuesUpdateQueueOutboundCallerConfigBodyParam = {
      OutboundCallerConfig: {}
    };
    describe('#updateQueueOutboundCallerConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateQueueOutboundCallerConfig(queuesInstanceId, queuesQueueId, queuesUpdateQueueOutboundCallerConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Queues', 'updateQueueOutboundCallerConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const queuesUpdateQueueStatusBodyParam = {
      Status: 'ENABLED'
    };
    describe('#updateQueueStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateQueueStatus(queuesInstanceId, queuesQueueId, queuesUpdateQueueStatusBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Queues', 'updateQueueStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const queuesCreateQueueBodyParam = {
      HoursOfOperationId: 'string',
      Name: 'string'
    };
    describe('#createQueue - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createQueue(queuesInstanceId, queuesCreateQueueBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Queues', 'createQueue', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeQueue - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeQueue(queuesInstanceId, queuesQueueId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.Queue);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Queues', 'describeQueue', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listQueueQuickConnects - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listQueueQuickConnects(queuesInstanceId, queuesQueueId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', typeof data.response.nextToken);
                assert.equal(true, Array.isArray(data.response.QuickConnectSummaryList));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Queues', 'listQueueQuickConnects', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const quickConnectsInstanceId = 'fakedata';
    const quickConnectsQuickConnectId = 'fakedata';
    const quickConnectsUpdateQuickConnectConfigBodyParam = {
      QuickConnectConfig: {}
    };
    describe('#updateQuickConnectConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateQuickConnectConfig(quickConnectsInstanceId, quickConnectsQuickConnectId, quickConnectsUpdateQuickConnectConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('QuickConnects', 'updateQuickConnectConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const quickConnectsUpdateQuickConnectNameBodyParam = {
      Description: 'string',
      Name: 'string'
    };
    describe('#updateQuickConnectName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateQuickConnectName(quickConnectsInstanceId, quickConnectsQuickConnectId, quickConnectsUpdateQuickConnectNameBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('QuickConnects', 'updateQuickConnectName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const quickConnectsCreateQuickConnectBodyParam = {
      Name: 'string',
      QuickConnectConfig: {}
    };
    describe('#createQuickConnect - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createQuickConnect(quickConnectsInstanceId, quickConnectsCreateQuickConnectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('QuickConnects', 'createQuickConnect', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listQuickConnects - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listQuickConnects(quickConnectsInstanceId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', typeof data.response.nextToken);
                assert.equal(true, Array.isArray(data.response.QuickConnectSummaryList));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('QuickConnects', 'listQuickConnects', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeQuickConnect - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeQuickConnect(quickConnectsInstanceId, quickConnectsQuickConnectId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.QuickConnect);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('QuickConnects', 'describeQuickConnect', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routingProfilesSummaryInstanceId = 'fakedata';
    describe('#listRoutingProfiles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listRoutingProfiles(routingProfilesSummaryInstanceId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', typeof data.response.nextToken);
                assert.equal(true, Array.isArray(data.response.RoutingProfileSummaryList));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RoutingProfilesSummary', 'listRoutingProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routingProfilesInstanceId = 'fakedata';
    const routingProfilesRoutingProfileId = 'fakedata';
    const routingProfilesAssociateRoutingProfileQueuesBodyParam = {
      QueueConfigs: [
        {}
      ]
    };
    describe('#associateRoutingProfileQueues - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.associateRoutingProfileQueues(routingProfilesInstanceId, routingProfilesRoutingProfileId, routingProfilesAssociateRoutingProfileQueuesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RoutingProfiles', 'associateRoutingProfileQueues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routingProfilesUpdateRoutingProfileConcurrencyBodyParam = {
      MediaConcurrencies: [
        {}
      ]
    };
    describe('#updateRoutingProfileConcurrency - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateRoutingProfileConcurrency(routingProfilesInstanceId, routingProfilesRoutingProfileId, routingProfilesUpdateRoutingProfileConcurrencyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RoutingProfiles', 'updateRoutingProfileConcurrency', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routingProfilesUpdateRoutingProfileDefaultOutboundQueueBodyParam = {
      DefaultOutboundQueueId: 'string'
    };
    describe('#updateRoutingProfileDefaultOutboundQueue - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateRoutingProfileDefaultOutboundQueue(routingProfilesInstanceId, routingProfilesRoutingProfileId, routingProfilesUpdateRoutingProfileDefaultOutboundQueueBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RoutingProfiles', 'updateRoutingProfileDefaultOutboundQueue', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routingProfilesDisassociateRoutingProfileQueuesBodyParam = {
      QueueReferences: [
        {}
      ]
    };
    describe('#disassociateRoutingProfileQueues - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.disassociateRoutingProfileQueues(routingProfilesInstanceId, routingProfilesRoutingProfileId, routingProfilesDisassociateRoutingProfileQueuesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RoutingProfiles', 'disassociateRoutingProfileQueues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routingProfilesUpdateRoutingProfileNameBodyParam = {
      Description: 'string',
      Name: 'string'
    };
    describe('#updateRoutingProfileName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateRoutingProfileName(routingProfilesInstanceId, routingProfilesRoutingProfileId, routingProfilesUpdateRoutingProfileNameBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RoutingProfiles', 'updateRoutingProfileName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routingProfilesUpdateRoutingProfileQueuesBodyParam = {
      QueueConfigs: [
        {}
      ]
    };
    describe('#updateRoutingProfileQueues - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateRoutingProfileQueues(routingProfilesInstanceId, routingProfilesRoutingProfileId, routingProfilesUpdateRoutingProfileQueuesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RoutingProfiles', 'updateRoutingProfileQueues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routingProfilesCreateRoutingProfileBodyParam = {
      DefaultOutboundQueueId: 'string',
      Description: 'string',
      MediaConcurrencies: [
        {}
      ],
      Name: 'string'
    };
    describe('#createRoutingProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createRoutingProfile(routingProfilesInstanceId, routingProfilesCreateRoutingProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RoutingProfiles', 'createRoutingProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeRoutingProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeRoutingProfile(routingProfilesInstanceId, routingProfilesRoutingProfileId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.RoutingProfile);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RoutingProfiles', 'describeRoutingProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRoutingProfileQueues - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listRoutingProfileQueues(routingProfilesInstanceId, routingProfilesRoutingProfileId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', typeof data.response.nextToken);
                assert.equal(true, Array.isArray(data.response.RoutingProfileQueueConfigSummaryList));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RoutingProfiles', 'listRoutingProfileQueues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityProfilesSummaryInstanceId = 'fakedata';
    describe('#listSecurityProfiles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listSecurityProfiles(securityProfilesSummaryInstanceId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', typeof data.response.nextToken);
                assert.equal(true, Array.isArray(data.response.SecurityProfileSummaryList));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityProfilesSummary', 'listSecurityProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tagsResourceArn = 'fakedata';
    const tagsTagResourceBodyParam = {
      tags: {}
    };
    describe('#tagResource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.tagResource(tagsResourceArn, tagsTagResourceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tags', 'tagResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTagsForResource - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listTagsForResource(tagsResourceArn, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.tags);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tags', 'listTagsForResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userHierarchyGroupsSummaryInstanceId = 'fakedata';
    describe('#listUserHierarchyGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listUserHierarchyGroups(userHierarchyGroupsSummaryInstanceId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', typeof data.response.nextToken);
                assert.equal(true, Array.isArray(data.response.UserHierarchyGroupSummaryList));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserHierarchyGroupsSummary', 'listUserHierarchyGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userHierarchyGroupsInstanceId = 'fakedata';
    const userHierarchyGroupsHierarchyGroupId = 'fakedata';
    const userHierarchyGroupsUpdateUserHierarchyGroupNameBodyParam = {
      Name: 'string'
    };
    describe('#updateUserHierarchyGroupName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateUserHierarchyGroupName(userHierarchyGroupsHierarchyGroupId, userHierarchyGroupsInstanceId, userHierarchyGroupsUpdateUserHierarchyGroupNameBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserHierarchyGroups', 'updateUserHierarchyGroupName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userHierarchyGroupsCreateUserHierarchyGroupBodyParam = {
      Name: 'string'
    };
    describe('#createUserHierarchyGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createUserHierarchyGroup(userHierarchyGroupsInstanceId, userHierarchyGroupsCreateUserHierarchyGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserHierarchyGroups', 'createUserHierarchyGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeUserHierarchyGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeUserHierarchyGroup(userHierarchyGroupsHierarchyGroupId, userHierarchyGroupsInstanceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.HierarchyGroup);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserHierarchyGroups', 'describeUserHierarchyGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userHierarchyStructureInstanceId = 'fakedata';
    const userHierarchyStructureUpdateUserHierarchyStructureBodyParam = {
      HierarchyStructure: {}
    };
    describe('#updateUserHierarchyStructure - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateUserHierarchyStructure(userHierarchyStructureInstanceId, userHierarchyStructureUpdateUserHierarchyStructureBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserHierarchyStructure', 'updateUserHierarchyStructure', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeUserHierarchyStructure - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeUserHierarchyStructure(userHierarchyStructureInstanceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.HierarchyStructure);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserHierarchyStructure', 'describeUserHierarchyStructure', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userInstanceId = 'fakedata';
    describe('#getFederationToken - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFederationToken(userInstanceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.Credentials);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'getFederationToken', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersSummaryInstanceId = 'fakedata';
    describe('#listUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listUsers(usersSummaryInstanceId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', typeof data.response.nextToken);
                assert.equal(true, Array.isArray(data.response.UserSummaryList));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UsersSummary', 'listUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersInstanceId = 'fakedata';
    const usersUserId = 'fakedata';
    const usersUpdateUserHierarchyBodyParam = {
      HierarchyGroupId: 'string'
    };
    describe('#updateUserHierarchy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateUserHierarchy(usersUserId, usersInstanceId, usersUpdateUserHierarchyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'updateUserHierarchy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUpdateUserIdentityInfoBodyParam = {
      IdentityInfo: {}
    };
    describe('#updateUserIdentityInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateUserIdentityInfo(usersUserId, usersInstanceId, usersUpdateUserIdentityInfoBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'updateUserIdentityInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUpdateUserPhoneConfigBodyParam = {
      PhoneConfig: {}
    };
    describe('#updateUserPhoneConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateUserPhoneConfig(usersUserId, usersInstanceId, usersUpdateUserPhoneConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'updateUserPhoneConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUpdateUserRoutingProfileBodyParam = {
      RoutingProfileId: 'string'
    };
    describe('#updateUserRoutingProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateUserRoutingProfile(usersUserId, usersInstanceId, usersUpdateUserRoutingProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'updateUserRoutingProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUpdateUserSecurityProfilesBodyParam = {
      SecurityProfileIds: [
        'string'
      ]
    };
    describe('#updateUserSecurityProfiles - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateUserSecurityProfiles(usersUserId, usersInstanceId, usersUpdateUserSecurityProfilesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'updateUserSecurityProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersCreateUserBodyParam = {
      PhoneConfig: {},
      RoutingProfileId: 'string',
      SecurityProfileIds: [
        'string'
      ],
      Username: samProps.authentication.username
    };
    describe('#createUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createUser(usersInstanceId, usersCreateUserBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'createUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeUser(usersUserId, usersInstanceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.User);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'describeUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInstance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteInstance(instanceInstanceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instance', 'deleteInstance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const instanceOrigin = 'fakedata';
    describe('#disassociateApprovedOrigin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.disassociateApprovedOrigin(instanceInstanceId, instanceOrigin, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instance', 'disassociateApprovedOrigin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIntegrationAssociation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIntegrationAssociation(instanceInstanceId, instanceIntegrationAssociationId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instance', 'deleteIntegrationAssociation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUseCase - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUseCase(instanceInstanceId, instanceIntegrationAssociationId, instanceUseCaseId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instance', 'deleteUseCase', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const instanceFunctionArn = 'fakedata';
    describe('#disassociateLambdaFunction - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.disassociateLambdaFunction(instanceInstanceId, instanceFunctionArn, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instance', 'disassociateLambdaFunction', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const instanceBotName = 'fakedata';
    const instanceLexRegion = 'fakedata';
    describe('#disassociateLexBot - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.disassociateLexBot(instanceInstanceId, instanceBotName, instanceLexRegion, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instance', 'disassociateLexBot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disassociateSecurityKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.disassociateSecurityKey(instanceInstanceId, instanceAssociationId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instance', 'disassociateSecurityKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disassociateInstanceStorageConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.disassociateInstanceStorageConfig(instanceInstanceId, instanceAssociationId, instanceResourceType, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instance', 'disassociateInstanceStorageConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteQuickConnect - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteQuickConnect(quickConnectsInstanceId, quickConnectsQuickConnectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('QuickConnects', 'deleteQuickConnect', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tagsTagKeys = [];
    describe('#untagResource - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.untagResource(tagsResourceArn, tagsTagKeys, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tags', 'untagResource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUserHierarchyGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUserHierarchyGroup(userHierarchyGroupsHierarchyGroupId, userHierarchyGroupsInstanceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserHierarchyGroups', 'deleteUserHierarchyGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUser(usersInstanceId, usersUserId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-amazon_connectservice-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'deleteUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
