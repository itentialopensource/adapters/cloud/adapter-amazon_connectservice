# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the AmazonConnectService System. The API that was used to build the adapter for AmazonConnectService is usually available in the report directory of this adapter. The adapter utilizes the AmazonConnectService API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The AWS Connect Service adapter from Itential is used to integrate the Itential Automation Platform (IAP) with AWS Connect Service. With this adapter you have the ability to perform operations such as:

- Get Contact Flows Summary
- Get Instance
- Create Instance
- Get Routing Profiles
- Get Security Profile Summary
- Tag Resource

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
