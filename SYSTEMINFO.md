# AWS Connect Service

Vendor: Amazon Web Services
Homepage: https://aws.amazon.com/

Product: Connect Service
Product Page: https://docs.aws.amazon.com/connect/

## Introduction
We classify AWS Connect Service into the Cloud domain as Connect Service provides Cloud Services.

"Amazon Connect is a contact center as a service (CCaaS) solution that offers easy, self-service configuration and enables dynamic, personal, and natural customer engagement at any scale." 

## Why Integrate
The AWS Connect Service adapter from Itential is used to integrate the Itential Automation Platform (IAP) with AWS Connect Service. With this adapter you have the ability to perform operations such as:

- Get Contact Flows Summary
- Get Instance
- Create Instance
- Get Routing Profiles
- Get Security Profile Summary
- Tag Resource

## Additional Product Documentation
The [API documents for AWS Connect Service](https://docs.aws.amazon.com/connect/latest/APIReference/Welcome.html)